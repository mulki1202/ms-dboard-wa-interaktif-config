def project = "ddb-development"
def appName = "ms-dboard-wa-interaktif"
def namespace = "sabrina-chatbot"
def proxyType = ""
def proxyAddress = ""
def proxyPort = ""
def IMAGE_TAG = ""
def CODE_REPO = "https://myogaaries07@bitbucket.org/bridce/ms-dboard-wa-interaktif.git"
def CREDENTIALS_CODE_REPO = "bitbucket-yoga"
def CONFIG_REPO = "https://myogaaries07@bitbucket.org/bridce/ms-dboard-wa-interaktif-config.git"
def CREDENTIALS_CONFIG_REPO = "bitbucket-yoga"
def SONARTOOL_NAME = "sonarscanner"
def BUILDER = "k8s-builder-prod"


pipeline {
  agent {
    kubernetes {
      defaultContainer "jnlp"
      yaml """
        apiVersion: v1
        kind: Pod
        metadata:
        labels:
          component: ci
        spec:
          tolerations:
          - key: "jenkins"
            operator: "Equal"
            value: "agent"
            effect: "NoSchedule"
          affinity:
            nodeAffinity:
              preferredDuringSchedulingIgnoredDuringExecution:
               - preference:
                   matchExpressions:
                   - key: jenkins
                     operator: In
                     values:
                     - agent
                 weight: 100
          hostAliases:
          - ip: "172.18.46.212"
            hostnames:
            - "sast.bri.co.id"
          serviceAccountName: cd2-jenkins
          containers:
          - name: gcloud
            image: google/cloud-sdk:263.0.0-alpine
            imagePullPolicy: IfNotPresent
            command:
            - cat
            tty: true
          - name: helm
            image: dtzar/helm-kubectl:2.14.1
            imagePullPolicy: IfNotPresent
            command:
            - cat
            tty: true
          - name: ansible
            image: fathoniadi/ansible:latest
            imagePullPolicy: IfNotPresent
            command:
            - cat
            tty: true
          - name: jnlp
            image: yogaarie/jnlp-yoga
            imagePullPolicy: IfNotPresent
        """
    }
  }
  
  
  
  
  stages {
    
    stage('clone') {
        steps {
            container('jnlp'){
                
                checkout([
                    $class: 'GitSCM', 
                    branches: [[name: params.BRANCH]],
                    extensions: [[
                        $class: 'RelativeTargetDirectory', 
                        relativeTargetDir: 'code']], 
                    userRemoteConfigs: [[
                        url: "${CODE_REPO}",
                        credentialsId: "${CREDENTIALS_CODE_REPO}",
                    ]]
                ])
                
                checkout([
                    $class: 'GitSCM', 
                    branches: [[name: 'refs/heads/master']],
                    extensions: [[
                        $class: 'RelativeTargetDirectory', 
                        relativeTargetDir: 'config']], 
                    userRemoteConfigs: [[
                        url: "${CONFIG_REPO}",
                        credentialsId: "${CREDENTIALS_CONFIG_REPO}",
                    ]]
                ])

            }
        }
    }
    
      
    stage('Quality Node') {
      when {
        expression {
         return ( params.DEPLOY_TO == "development" || params.DEPLOY_TO == "uat"  )
        }
      }
      environment {
        scannerHome = tool "${SONARTOOL_NAME}"
      }
      steps {
        dir('code') {
            withSonarQubeEnv('sonarqube') {
            sh "${scannerHome}/bin/sonar-scanner -X"
          }
        }
      }
    }
    stage("build image") {
      environment {
        IMAGE_REPO = "gcr.io/${project}/${appName}"
      }  
      steps {
        container("gcloud") {
          withCredentials([file(credentialsId: "${BUILDER}" , variable: "builder")]) {
            dir('code') {
               script { 
                   
                    sh 'git rev-parse --short HEAD > .git/commit-id'
                    
                    def commit_id = readFile('.git/commit-id').trim()
                    IMAGE_TAG = commit_id.substring(0,7)
                    
                 
                    if(proxyType != ""){
                      sh "gcloud config set proxy/type ${proxyType}"
                      sh "gcloud config set proxy/address ${proxyAddress}"
                      sh "gcloud config set proxy/port ${proxyPort}"
                    }

                   
    
                    writeFile file: 'key.json', text: readFile(builder)
    
    
                    sh "gcloud auth activate-service-account --key-file=key.json"
    
                    sh "gcloud builds submit --project ${project} --tag ${IMAGE_REPO}:${IMAGE_TAG} ."
               }
                
            }
          }
        }
      }
    }


    stage("Deploy to Dev") {
      when {
        expression {
         return ( params.DEPLOY_TO == "development"  )
        }
      }
      environment {
        IMAGE_REPO = "gcr.io/${project}/${appName}"
      }
      steps {
        container("helm") {
          script{
            for(kubeconfig_selected in params.KUBECONFIGS.split(',')) {
              if(kubeconfig_selected){
                withCredentials([file(credentialsId: "${kubeconfig_selected}", variable: "KUBECONFIG")]) {
                  dir('config'){
                  
                    echo "Deploy to cluster ${kubeconfig_selected}"
                    sh "mkdir -p ~/.kube/"
                    writeFile file: '~/.kube/config', text: readFile(KUBECONFIG)
                  
                    sh "cp ../code/.env-dev helm/${appName}/config/secrets/.env"

                    sh """
                    
                        helm upgrade ${appName} ./helm/${appName} \
                        --set-string image.repository=${IMAGE_REPO},image.tag=${IMAGE_TAG} \
                        -f ./helm/values.dev.yml --debug --install --namespace ${namespace}
                    """
                  }
                }
              }
            }
          }
        }
      }
    }
    stage("Deploy to UAT") {
      when {
        expression {
         return ( params.DEPLOY_TO == "uat"  )
        }
      }
      environment {
        IMAGE_REPO = "gcr.io/${project}/${appName}"
      }
      steps {
        container("helm") {
          withCredentials([file(credentialsId: "${KUBECONFIG_ENV}", variable: "KUBECONFIG")]) {

            sh "mkdir -p ~/.kube/"
            writeFile file: '~/.kube/config', text: readFile(KUBECONFIG)
            

            sh """
              
              helm upgrade ${appName}-uat ./helm/${appName} \
              --set-string image.repository=${IMAGE_REPO},image.tag=${IMAGE_TAG} \
              -f ./helm/values.uat.yml --debug --install --namespace ${namespace}-uat
            """
          }
        }
      }
    }

    stage("Deploy to PROD") {
      when {
        expression {
         return ( params.DEPLOY_TO == "production"  )
        }
      }
      environment {
        IMAGE_REPO = "gcr.io/${project}/${appName}"
      }
      steps {
        container("helm") {
          withCredentials([file(credentialsId: "${KUBECONFIG_ENV}", variable: "KUBECONFIG")]) {

            sh "mkdir -p ~/.kube/"
            writeFile file: '~/.kube/config', text: readFile(KUBECONFIG)

            
            sh """
              
              helm upgrade ${appName}-production ./helm/${appName} \
              --set-string image.repository=${IMAGE_REPO},image.tag=${IMAGE_TAG} \
              -f ./helm/values.prod.yml --debug --install --namespace ${namespace}-production
            """
          }
        }
      }
    }

    stage('SAST') { 
      when {
        expression {
         return (  params.SCAN_SAST == "yes"  )
        }
      }
    steps {
      script {
          step([$class: 'CxScanBuilder', comment: '', credentialsId: '', excludeFolders: 'helm, vendor', exclusionsSetting: 'job', failBuildOnNewResults: false, failBuildOnNewSeverity: 'HIGH', filterPattern: '!.* ', fullScanCycle: 10, generatePdfReport: true, groupId: '2d95991a-f4d4-43f6-8cb8-4029b9ab4410', incremental: true, password: '{AQAAABAAAAAQ6aiwQviXnYu3oM9JZHpd8ZG+N1gQtlTUD76ogPfFv00=}', preset: '100065', projectName: "$appName", sastEnabled: true, serverUrl: 'https://sast.bri.co.id', sourceEncoding: '1', username: '', vulnerabilityThresholdResult: 'FAILURE', waitForResultsEnabled: false])
        }
      }
    }
  }
}